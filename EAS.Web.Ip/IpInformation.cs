﻿using System.Net;
using Newtonsoft.Json;

namespace EAS.Web.Ip
{
    public class IpInformation
    {
        /// <summary>
        /// Returns object, containing information about location of the given Ip.
        /// <para>Note: 10,000 findings per hour.</para>
        /// </summary>
        /// <param name="ipAddress">8.8.8.8 or google.com</param>
        /// <returns>Object containing information about location.</returns>
        public static IpLocation GetIpLocation(string ipAddress)
        {
            //  freegeoip.net/json/IP

            ipAddress = ipAddress == "::1" ? "127.0.0.1" : ipAddress;
            string url = @"https://freegeoip.net/json/" + ipAddress;

            using (WebClient webClient = new WebClient())
            {
                var json = webClient.DownloadString(url);

                var ipLocation = JsonConvert.DeserializeObject<IpLocation>(json);
                return ipLocation;
            }
        }
    }
}
